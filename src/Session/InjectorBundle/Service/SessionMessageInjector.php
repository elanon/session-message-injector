<?php

namespace Session\InjectorBundle\Service;

use Session\InjectorBundle\Event\InjectMessageEvent;
use Session\InjectorBundle\Listener\InjectMessageListener;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SessionMessageInjector
{

    const EVENT_NAME = 'session.inject.message';
    const METHOD_NAME = 'injectMessage';

    protected $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->registerListeners();
    }

    private function registerListeners()
    {
        $messageListener  = new InjectMessageListener();
        $this->eventDispatcher->addListener(self::EVENT_NAME, array($messageListener, self::METHOD_NAME));
    }

    public function injectMessage(InjectMessageEvent $event)
    {
        $this->eventDispatcher->dispatch(self::EVENT_NAME, $event);
    }

}