<?php

namespace Session\InjectorBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Process\Process;

class InjectMessageEvent extends Event
{
    private $sessionId;
    private $command;
    private $parameters;

    public function __construct($sessionId, $command, $parameters)
    {
        $this->sessionId = $sessionId;
        $this->command = $command;
        $this->parameters = json_encode($parameters);
    }

    public function runCommand()
    {
        $cmd = 'php ../bin/console inject:session:message -e prod ';
        //todo add session_save_path to parameters
        $cmd .= implode(' ', [$this->sessionId, session_save_path(), $this->command, "'".$this->parameters."'"]);
        $process = new Process($cmd);
        $process->disableOutput();
        $process->start();
    }
}