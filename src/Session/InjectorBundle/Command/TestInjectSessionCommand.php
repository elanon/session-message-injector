<?php

namespace Session\InjectorBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestInjectSessionCommand extends Command
{
    protected function configure()
    {
        $this->setName('test:session:inject')
            ->setDescription('test inject data to session')
            ->setDefinition([
            ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        sleep(10);
        $output->write(json_encode(['type' => 'success', 'message' => 'Its WORKING!']));
    }
}