<?php

namespace Session\InjectorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class InjectOutputToSessionCommand extends ContainerAwareCommand
{

    private $command;
    private $params;
    private $sessionId;
    private $sessionPath;
    private $session;


    protected function configure()
    {
        $this->setName('inject:session:message')
            ->setDescription('Command to inject a output message to session')
            ->setDefinition([
                new InputArgument('sessionId', InputArgument::REQUIRED, 'ID of currect session user'),
                new InputArgument('sessionPath', InputArgument::REQUIRED, 'Path to session storage'),
                new InputArgument('cmdToExecute', InputArgument::REQUIRED, 'Command to execute.'),
                new InputArgument('params', InputArgument::OPTIONAL, "Json of array params key => value")
            ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        try{
            $buffer = new BufferedOutput();
            $this->initalize($input);
            $command = $this->getApplication()->find($this->command);
            $this->params['command'] = $this->command;
            $commandArgs = new ArrayInput($this->params);
            $command->run($commandArgs, $buffer);
            $returnValue = $buffer->fetch();
            $returnValue = json_decode($returnValue, true);
            if (is_array($returnValue) && isset($returnValue['type']) && isset($returnValue['message'])) {
                $this->injectMessageToSession($returnValue);
            } else {
                throw new CommandNotFoundException("Command not found or parameters are not returned");
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    private function initalize(InputInterface $input)
    {
        session_destroy();
        $this->sessionId = $input->getArgument('sessionId');
        $this->command = $input->getArgument('cmdToExecute');
        $this->params = json_decode($input->getArgument('params'), true);
        $this->sessionPath = $input->getArgument('sessionPath');
        ini_set('session.save_path', $this->getContainer()->getParameter('session_save_path') ? $this->getContainer()->getParameter('session_save_path') : $this->sessionPath);
        ini_set('session.save_handler', $this->getContainer()->getParameter('session_save_handler') ? $this->getContainer()->getParameter('session_save_handler') : 'files');

        session_id($this->sessionId);
    }

    private function injectMessageToSession($returnValue)
    {
        $this->session = new Session();
        $this->session->setId($this->sessionId);
        $this->session->start();
        $this->session->getFlashBag()->add($returnValue['type'], $returnValue['message']);
        $this->session->save();
    }
}
