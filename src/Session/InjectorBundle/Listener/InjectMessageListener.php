<?php

namespace Session\InjectorBundle\Listener;


use Session\InjectorBundle\Event\InjectMessageEvent;

class InjectMessageListener
{
    public function injectMessage(InjectMessageEvent $event)
    {
        $event->runCommand();
    }
}