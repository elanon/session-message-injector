Session Message injector
========================

For what?
--------------
For inject flash message to session. Example generate big file and infrom user when is it completed with download link.

Example
--------------
        $sessionInjector = $this->get('session_injector');
        $event = new InjectMessageEvent(
            $request->getSession()->getId(),
                'test:session:inject',
                ['test1'=>'arguemntForTest1', 'test2' => 'arguemntForTest2']
        );
        $sessionInjector->injectMessage($event);


Example command
--------------

    protected function configure()
    {
        $this->setName('test:session:inject')
            ->setDescription('test inject data to session')
            ->setDefinition([
                new InputArgument('test1', InputArgument::REQUIRED, 'Language'),
                new InputArgument('test2', InputArgument::REQUIRED, 'Shop urlname'),
            ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $test1 = $input->getArgument('test1');
        $test2 =  $input->getArgument('test2');
        sleep(10);
        $output->write(json_encode(['type' => 'success', 'message' => 'Its WORKING!']));
    }

